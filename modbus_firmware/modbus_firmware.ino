#include <ModbusRtu.h>

uint16_t au16data[16];

#define LED_PIN 13
#define BUTTON_PIN 0

Modbus slave(1,Serial,0);

#include "MbMap.h"
ModbusMap mb_map(au16data);

#define EMBED_START 0
#define BUTTON_INPUT_START 4
#define BUTTON_OUTPUT_START 8

#define FLOOR_PIN 13
#define POOFER_0_PIN 14
#define POOFER_1_PIN 15
#define LIGHTS_PIN 16

void setup() {

#include "mb_map.h"

  Serial.begin(115200);
  slave.start();
}

void loop() {
  slave.poll( au16data, 16 );
  mb_map.update();
}
