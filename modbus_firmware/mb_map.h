	mb_map.add_input("EMBED_0", 0, 0, 0, INPUT_PULLUP, true);
	mb_map.add_input("BUTTON_INPUT_0", 1, 0, 4, INPUT_PULLUP, true);
	mb_map.add_input("EMBED_1", 0, 1, 1, INPUT_PULLUP, true);
	mb_map.add_input("BUTTON_INPUT_1", 1, 1, 5, INPUT_PULLUP, true);
	mb_map.add_input("EMBED_2", 0, 2, 2, INPUT_PULLUP, true);
	mb_map.add_input("BUTTON_INPUT_2", 1, 2, 6, INPUT_PULLUP, true);
	mb_map.add_input("EMBED_3", 0, 3, 3, INPUT_PULLUP, true);
	mb_map.add_input("BUTTON_INPUT_3", 1, 3, 7, INPUT_PULLUP, true);
	mb_map.add_coil("BUTTON_OUTPUT_0", 4, 0, 8, LOW);
	mb_map.add_coil("BUTTON_OUTPUT_1", 4, 1, 9, LOW);
	mb_map.add_coil("BUTTON_OUTPUT_2", 4, 2, 10, LOW);
	mb_map.add_coil("BUTTON_OUTPUT_3", 4, 3, 11, LOW);
	mb_map.add_coil("FLOOR", 5, 0, 13, LOW);
	mb_map.add_coil("POOFER_0", 5, 1, 14, LOW);
	mb_map.add_coil("POOFER_1", 5, 2, 15, LOW);
	mb_map.add_coil("LIGHTS", 5, 3, 16, LOW);

