struct ModbusMapPin {
  int mode;
  uint16_t reg;
  uint8_t bit;
  uint8_t pin;
  bool invert;
};

class ModbusMap {
public:
  ModbusMap(uint16_t *_data) {
    data = _data;
  }
  void add_coil(char *name, uint16_t _reg, uint8_t _bit, uint8_t _pin, int _state=LOW) {
    pinMode(_pin, OUTPUT);
    digitalWrite(_pin, _state);
    ModbusMapPin p{ OUTPUT, _reg, _bit, _pin };
    map[length++] = p;
  }

  void add_input(char *name, uint16_t _reg, uint8_t _bit, uint8_t _pin, int _mode, bool _invert=false) {
    pinMode(_pin, _mode);
    ModbusMapPin p{ _mode, _reg, _bit, _pin, _invert };
    map[length++] = p;
  }

  void update() {
    for (int i=0; i<length; i++) {
      ModbusMapPin p = map[i];
      if (p.mode == OUTPUT) {
        digitalWrite(p.pin, bitRead(data[p.reg], p.bit));
      } else {
        int val = digitalRead(p.pin);
        if (p.invert) val = !val;
        bitWrite(data[p.reg], p.bit, val);
      }
    }
  }
private:
  uint16_t *data;
  ModbusMapPin map[64];
  int length = 0;
};
