import glfw
import OpenGL.GL as gl
import imgui
from imgui.integrations.glfw import GlfwRenderer
from .display import WIDTH, HEIGHT, impl_glfw_init
import serial.tools.list_ports

class GUI:
  def __init__(self, mb, update, draw):
    self.mb = mb
    self.user_update = update
    self.user_draw = draw

  def main(self):
    self.running = True
    self.window = impl_glfw_init(self.on_key)
    imgui.create_context()
    self.impl = GlfwRenderer(self.window)

    self.cur_serial_port_idx = 0
    self.available_ports = serial.tools.list_ports.comports()

    while not glfw.window_should_close(self.window):
      self.loop()

    self.impl.shutdown()
    glfw.terminate()

  def loop(self):

    if self.user_update:
      self.user_update()

    glfw.poll_events()
    self.impl.process_inputs()

    imgui.new_frame()
    imgui.set_next_window_size(WIDTH, HEIGHT)
    imgui.set_next_window_position(0,0)

    self.draw()

    gl.glClearColor(1., 1., 1., 1)
    gl.glClear(gl.GL_COLOR_BUFFER_BIT)

    imgui.render()
    self.impl.render(imgui.get_draw_data())
    glfw.swap_buffers(self.window)

  def draw(self):
    imgui.begin(f"modbus_multitool")

    expanded, visible = imgui.collapsing_header("Device", None, imgui.TREE_NODE_DEFAULT_OPEN)
    if expanded:
      if not self.mb.connected:
        clicked, self.cur_serial_port_idx = imgui.combo(
          "serial port", self.cur_serial_port_idx, ["none", *[d.name for d in self.available_ports]]
        )

        if clicked:
          port = self.available_ports[self.cur_serial_port_idx-1] # account for "none" in list
          self.set_port(port.device)
      else:
        imgui.text("CONNECTED!")

    if self.mb.connected:
      expanded, visible = imgui.collapsing_header("Inputs/Outputs", None, imgui.TREE_NODE_DEFAULT_OPEN)
      if expanded:
        imgui.text("Inputs")
        imgui.separator()
        for inp in self.mb.inputs.values():
          name = inp.name
          state = inp.get()
          imgui.checkbox(name, state)

        imgui.text("Floats")
        imgui.separator()
        for float_input in self.mb.float_inputs.values():
          name = float_input.name
          value = float_input.get()
          imgui.input_float(name, value)


        imgui.text("Outputs")
        imgui.separator()
        for outp in self.mb.coils.values():
          name = outp.name
          state = outp.get()
          clicked, new_state = imgui.checkbox(name, state)
          if clicked:
            outp.set(new_state)

    if self.user_draw:
      self.user_draw()

    imgui.end()

  def on_key(self, *args, **kwargs):
    log("ONKEY", args, kwargs)

  def set_port(self, filename):
    self.mb.connect(filename)
