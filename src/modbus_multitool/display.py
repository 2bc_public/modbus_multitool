import glfw
import OpenGL.GL as gl

WIDTH = 1200
HEIGHT = 1080

def impl_glfw_init(on_key):
  width, height = WIDTH, HEIGHT
  window_name = "modbus_multitool"

  if not glfw.init():
    print("Could not initialize OpenGL context")
    exit(1)

  # OS X supports only forward-compatible core profiles from 3.2
  glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
  glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3)
  glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)

  glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, gl.GL_TRUE)


  # Create a windowed mode window and its OpenGL context
  window = glfw.create_window(
    int(width), int(height), window_name, None, None
  )
  #glfw.set_key_callback(window, on_key)
  glfw.make_context_current(window)

  if not window:
    glfw.terminate()
    print("Could not initialize Window")
    exit(1)

  return window
