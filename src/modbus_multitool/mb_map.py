import jinja2
import minimalmodbus
from collections import defaultdict
from dataclasses import dataclass
import re

LOW = "LOW"
HIGH = "HIGH"
INPUT = "INPUT"
INPUT_PULLUP = "INPUT_PULLUP"

@dataclass
class Coil:
  name: str
  register: int
  bit: int
  pin: int
  parent: object = None

  def set(self, value):
    value = value and 1 or 0
    self.parent.dev.write_bit(self.register*16+self.bit, value)

  def get(self):
    return self.parent.dev.read_bit(self.register*16+self.bit, 1)

  def setup(self):
    return jinja2.Template("""
      pinMode({{pin}}, OUTPUT);
    """).render(**self.__dict__)

  def update(self):
    return jinja2.Template("""
      digitalWrite({{pin}}, bitRead(au16data[{{register}}], {{bit}}));
    """).render(**self.__dict__)

@dataclass
class Input:
  name: str
  register: int
  bit: int
  pin: int
  mode: str
  invert: str
  parent: object = None
  keyboard: str = None

  def get(self):
    return self.parent.dev.read_bit(self.register*16+self.bit, 1)

  def setup(self):
    return jinja2.Template("""
      pinMode({{pin}}, {{mode}});
      {{name}}__bounce.attach ({{pin}}, {{mode}});
      {{name}}__bounce.interval(25);
      {{name}}__bounce.setPressedState(!{{invert|lower}});

    """).render(**self.__dict__)

  def update(self):
    return jinja2.Template("""
      {
        {{name}}__bounce.update();
        int val = {{name}}__bounce.read();
        if ({{invert|lower}}) val = !val;
        bitWrite(au16data[{{register}}], {{bit}}, val);

        {% if keyboard %}
        if ({{name}}__bounce.pressed()) Keyboard.print("{{keyboard}}");
        if ({{name}}__bounce.released()) {
          Keyboard.press(MODIFIERKEY_SHIFT);
          Keyboard.print("{{keyboard}}");
          Keyboard.release(MODIFIERKEY_SHIFT);
        }
        {% endif %}
      }
    """).render(**self.__dict__)

@dataclass
class FloatInput:
  name: str
  register: int
  pin: object = None
  resolution: int = 10
  byteorder: int = minimalmodbus.BYTEORDER_LITTLE_SWAP
  parent: object = None

  def get(self):
    return self.parent.dev.read_float(self.register, byteorder=self.byteorder)

  def setup(self):
    return ""

  def update(self):
    if self.pin is None:
      return ""
    return jinja2.Template("""
      set_float({{register}}, 1.0*analogRead({{pin}})/(1<<{{resolution}}));
    """).render(**self.__dict__)

class MBMap:
  def __init__(self, defines={}):
    self.inputs = {}
    self.float_inputs = {}
    self.coils = {}
    self.defines = defines

    self.connected = False

    self.last_state = defaultdict(int)

  def add(self, item):
    item.parent = self
    if isinstance(item, Input):
      self.inputs[item.name] = item
    elif isinstance(item, FloatInput):
      self.float_inputs[item.name] = item
    elif isinstance(item, Coil):
      self.coils[item.name] = item
    setattr(self, item.name, item)

  def get_state(self):
    if not self.connected:
      return None

    inputs = {}
    coils = {}

    for inp in self.inputs.values():
      name = inp.name
      state = inp.get()
      d = {
        "name": name,
        "state": state,
        "last_state": self.last_state[name],
      }
      inputs[name] = d
      self.last_state[name] = state

    for coil in self.coils.values():
      name = coil.name
      state = coil.get()
      d = {
        "name": name,
        "state": state,
      }
      coils[name] = d

    return {
      "inputs": inputs,
      "coils": coils
    }

  def monitor_input_events(self, *args, **kwargs):
    if not self.connected:
      self.connect(*args, **kwargs)
    while True:
      for inp in self.inputs.values():
        name = inp.name
        state = inp.get()
        if state != self.last_state[name]:
          print(name, state and "ROSE" or "FELL")
        self.last_state[name] = state

    
  def to_c(self):
    c = jinja2.Template("""
    #include "ModbusRtu.h"
    #include <Bounce2.h>
    using namespace Bounce2;

    {% for name, input in inputs.items() %}
    Button {{name}}__bounce = Button();
    {% endfor %}

    {% for name, float_input in float_inputs.items() %}
    #define {{name}} {{float_input.register}}
    {% endfor %}

    {% for name, value in defines.items() %}
    #define {{name}} {{value}}
    {% endfor %}

    Modbus slave(1,Serial,0);
    uint16_t au16data[64];

    void set_float(int _register, float value) {
      float *p = (float *)(&au16data[_register]);
      *p = value;
    }

    void modbus_setup() {
      Serial.begin(115200);
      slave.start();

      //// INPUTS
      {% for name, input in inputs.items() %}
      // {{name}}
      {{input.setup()}}
      {% endfor %}

      //// COILS
      {% for name, coil in coils.items() %}
      // {{name}}
      {{coil.setup()}}
      {% endfor %}
    }

    void modbus_update() {
      slave.poll( au16data, 64 );

      //// INPUTS
      {% for name, input in inputs.items() %}
      // {{name}}
      {{input.update()}}
      {% endfor %}

      //// COILS
      {% for name, coil in coils.items() %}
      // {{name}}
      {{coil.update()}}
      {% endfor %}

      //// FLOATS
      {% for name, float_input in float_inputs.items() %}
      // {{name}}
      {{float_input.update()}}
      {% endfor %}
    }

    """).render(
      defines=self.defines,
      inputs=self.inputs,
      coils=self.coils,
      float_inputs=self.float_inputs
    )
    c = re.sub(r"^    ", "", c, flags=re.MULTILINE)

    print(c)
      
  def connect(self, port):
    self.dev = minimalmodbus.Instrument(port, 1)  # port name, slave address (in decimal)
    self.dev.serial.baudrate = 115200
    #self.dev.debug = True
    self.connected = True
